import Database from './db/database'
import Login from './components/login'
import Signup from './components/signup'
import Profile from './components/profile'
import Options from './components/options'
import Helpers from './helpers'

/*
  Define const vars.

  Required files
    Database: LocalStorage DB
    Login: Login class Instance
    Signup: Signup class Instance
    Profile: Profile class Instance
    Options: Options class Instance
    Helpers: Helpers class Instance
  
  Class Methods
    saveUser: Allow Signup
    login: Allow Login
    logout: Destroy session
    edit: Create Edit Box
    cancelEdit: Destroy Edit Box
    updateUser: Save new data to DB
*/
const database = new Database('alexander_test'),
      home = new Login(database),
      helpers = new Helpers(),
      signup = new Signup(database),
      profile = new Profile(helpers, database),
      options = new Options(helpers),
      saveUser = () => signup.signup(),
      login = () => home.login(),
      logout = () => profile.logout(),
      edit = () => profile.edit(),
      cancelEdit = () => profile.cancelEdit(),
      updateUser = () => profile.saveEdit();

/*
  oldView: Save old view to avoid unnecessary calls to setView()
  currentWidth: Detect screen size
  swapZoneMobile: Draggable container, for swap in devices under 768px
  swapZoneDesktop: Container for swap on clicks, Desktop
  swapOption: Trigger Swap change
  active: Swap option selected
  start: Get start offset of swap container
  stop: Get stop offset of swap container
  optionIndex
*/
let oldView = '',
    currentWidth = 'na',
    swapZoneMobile = '.data-edit--mobile',
    swapZoneDesktop = '.data-edit',
    swapOption = '.swap_option',
    active = '.swap_option.active',
    start,
    stop,
    optionIndex;

$(window).on('load hashchange resize', (e) => {
  e.preventDefault;

  // Define view variable for setView() method of each controller
  let setView,
      size = helpers.getSize();

  // If session exist, we send user to profile
  if ($.session.get("user") != '' && $.session.get("user") != null) {

    // Profile Instance
    let view = new Profile(helpers, database);

    // control screen size
    if (currentWidth != size) {

      // Set profile url
      window.history.pushState("", "", '#user/profile');

      // set Old view
      oldView = 'user/profile';

      // Set current width to avoid unnecessary calls to setView()
      currentWidth = size;

      // Assign setView() method to var
      setView = () => view.show();
      setView();   
    }
  } else {

    //Get url
    let rawView = document.location.hash,
        viewName = rawView.replace(/^#\//, '');

    /* 

      Call to setView() method


      Don't setView if:
        We are in the same url

      SetView if:
        We don't have viewName -> Load Default view
        viewName if profile -> because we call to diffferent views depending on screen width
    */
    if (!viewName == oldView || viewName == '' || viewName == 'user/profile') {
      switch(viewName) {
        case 'signup': {
          let view = signup;
          setView = () => view.show();
          break; 
        }
        case 'user/profile': {
          window.location.replace("");
          break; 
        } 
        default: {
          let view = home;
          setView = () => view.show();
          break; 
        }     
      }
      oldView = viewName;
      setView();
    }    
  }

  // If we are in mobile activate draggable container
  if ($(window).width() < 961) {
    interact(swapZoneMobile)
      .draggable({
        // enable inertial throwing
        inertia: true,
        // keep the element within the area of it's parent
        axis: "x",
        restrict: {
          restriction: "parent"
        },
        // enable autoScroll
        autoScroll: true,
        onstart: () => {
          start = $(swapZoneMobile).offset().left;
        },
        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: (event) => {
          let stop = $(swapZoneMobile).offset().left;
          if(stop > start){
            optionIndex = $(active).index() + 1;
          } else {
            optionIndex = (($(active).index() > 0) ? $(active).index() - 1 : 0);
          } 
          showOptionView();
        }
      });
  }
});

// Dragg in mobile
function dragMoveListener (event) {
  var target = event.target,
      // keep the dragged position in the data-x/data-y attributes
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform =
  target.style.transform =
    'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

// Get optionView on click
let showOptionView = () => {
  $(swapOption).removeClass('active');
  $(swapOption).eq(optionIndex).addClass('active');
  $(swapZoneMobile).children().fadeOut("fast");
  options.getOptionView($(swapOption).eq(optionIndex).attr('name'), '.main-section');
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

// Click events for actions
$(document).on('click', '.default-button__login-form', login);
$(document).on('click', '.default-button__signup-form', saveUser);
$(document).on('click', '.ion-edit.-edit', edit);
$(document).on('click', '#cancel_save', cancelEdit);
$(document).on('click', '#save', updateUser);
$(document).on('click', '#logout', logout);
$(document).on('click', swapOption, (e) => {
  e.preventDefault;
  $(swapOption).removeClass('active');
  $(e.target).addClass('active');
  $(swapZoneDesktop).children().fadeOut("fast");
  console.log($(e.target));
  options.getOptionView($(e.target).attr('name'), '.main-section');
});