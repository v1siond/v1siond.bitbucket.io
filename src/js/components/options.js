export default class Options {

  constructor(helpers) {
    /*
      @params:
        helpers: Access to helper methods

      user: Current user
      path: path method from helpers
      data: view variable keys
      values: define User data
    */
    this.helpers = helpers;
    this.user = this.helpers.getUser();
    this.path = () => this.helpers.getPath();
    this.data = helpers.exportViewVariables('Options');

    // If no one's logged, then assign na
    this.values = ((this.user == 'na') ? '' : [this.user.name, this.user.email, this.user.webpage, this.user.phone, this.user.direction]);
    ;  
  }  

  /*
    We get the view name and the container where its going to be appended, modifying data keys to show correct user data
  */
  getOptionView(viewName, container) {
    // create view path and get it with getRemote -> a helper method
    let markupUrl = this.path() + viewName + '.html',
        view = this.helpers.getRemote(markupUrl);
    // Modify data keys for values
    view = this.helpers.insertVars(this.data, this.values, view);

    // Mobile have different markup structure, so we check that in order to get correct view
    if (view == 'about' && this.helpers.getSize() == 'mobile') {
      $(container).remove();
      return $('.view-wrapper').append(view);
    } else {
      return $(container).html(view);
    }
  }
}