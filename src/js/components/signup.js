import Header from './header'

export default class Signup { 

  constructor(database) {
    /*
      @params
        database: our localStorage DB
      name: Module name
      headerClass: Header instance
      header: header markup
    */
    this.database = database;
    this.name = 'signup';
    this.headerClass = new Header(this.name);
    this.header = this.headerClass.export()
  }

  /*
    get view and append to principal container
  */
  show() {
    let container = $('.view-wrapper'),
        view;
    $.get('src/views/signup/signup.html', (data) => {
      view = data;
      let markup =  `
        ${this.header}
         <main class="flexbox-container flexbox-container--center-horizontal flexbox-container--vertical-start main-section--home">
            ${view}
         </main>
         <footer class="footer">
          <p class="text">MIT license - Alexander Pulido
          </p>
        </footer>
      `;
      return container.html(markup).removeClass('view-wrapper--home view-wrapper--profile').addClass('view-wrapper--signup');
    });
  }

  /*
    get input values and convert them to a stringify json
  */
  signup() {
    let user = JSON.stringify({
      id: Date.now(),
      username: $('#user-name').val(),
      password: $('#password').val(),
      email: $('#email').val(),
      name: $('#name').val(),
      direction: $('#direction').val(),
      phone: $('#phone').val(),
      webpage: $('#web-page').val(),
      followers: 0,
      reviews: 0,
      image: ''
    });
    return this.database.insert(user);
  }

}
