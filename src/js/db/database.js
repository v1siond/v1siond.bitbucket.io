export default class Database {

  constructor(name, callback) {
    const localStorage = window.localStorage;

    let liveTodos;

    /**
     * Read the local UserList from localStorage.
     *
     * @returns array of todos
     */
    this.getLocalStorage = () => {
      return liveTodos || JSON.parse(localStorage.getItem(name) || '[]');
    };

    /**
     * Write the local UserList to localStorage.
     *
     */
    this.setLocalStorage = (todos) => {
      localStorage.setItem(name, JSON.stringify(liveTodos = todos));
    };

    if (callback) {
      callback();
    }
  }

  /*
    Find users with properties matching those on query.
    @param
     username
     password
  */
  find(username, password) {

    // get DB
    const todos = this.getLocalStorage();
    let exist = false,
        userData;

    // Run through DB
    $.each(todos, (index, val) => {

      // Get item user data
      let user = JSON.parse(val);

      // If user info match data -> get all values and set exist = true
      if (username == user.username &&  password == user.password ) {
        userData = JSON.stringify({id: user.id, username: user.username, password: user.password, email: user.email, name: user.name, direction: user.direction, phone: user.phone, webpage: user.webpage, followers: user.followers, reviews: user.reviews});
        exist = true;
      };
    });

    // check if user exist, create session if true
    if (exist == false) {
      alert('User not found');
    } else {
      alert('Welcome ' + username);
      $.session.set("user", userData);
      window.location.replace("");
    }
  }

  /**
   * Update an user in the Database.
   *
   * @params
      id: user id
      update: new data
      fieldName: array of matching keys
   */
  update(id, update, fieldName, callback) {

    // Get DB
    const todos = this.getLocalStorage();

    // User json
    let newData = JSON.parse(update),
    userData;

    // Run through DB
    $.each(todos, (index, val) => {

      // Get current item json
      let user = JSON.parse(val);

      // If user exist, get keys
      if (user.id == id) {

        // Get keys and assign them to a variable
        let keys = Object.keys(user);

        // look for matching keys and modify value when find one
        for (var i = 0; i < keys.length; i++) {
          if (keys[i] == fieldName) {
            user[keys[i]] = newData[fieldName];
            alert('Successful Update!');
          } else {
            for (var j = 0; j < fieldName.length; j++) {
              if (keys[i] == fieldName[j]) {
                user[keys[i]] = newData[keys[i]];
              }
            }
          }
        }
        // Create new User List
        userData = JSON.stringify({id: user.id, username: user.username, password: user.password, email: user.email, name: user.name, direction: user.direction, phone: user.phone, webpage: user.webpage, followers: user.followers, reviews: user.reviews});
      };
    });

    // Set DB with new values
    this.setLocalStorage(todos);
    $.session.set("user", userData);

    if (callback) {
      callback();
    } else {
      window.location.replace("");
    }
  }

  /*
   Insert an user into the Database.
   @param
    user: user to insert
  */
  insert(user) {

    //get DB
    const todos = this.getLocalStorage();

    // Get new user data
    let checkUser = JSON.parse(user),
        exist = false;

    // Run through DB
    $.each(todos, (index, val) => {

      // Get current item json
      let user = JSON.parse(val);

      // Check if user exist
      if (checkUser.username == user.username || checkUser.email == user.email ) {
        exist = true;
      };
    });

    // If user exist, don't save
    if (exist == false) {
      todos.push(user);
      alert('Successful Signup!');
    } else {
      alert(checkUser.username + 'User already exist');
    }

    // set DB with new user
    this.setLocalStorage(todos);
    window.location.replace("");
  }
}