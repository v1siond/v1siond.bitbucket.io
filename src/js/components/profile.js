import Header from './header'

export default class Profile { 

  constructor(helpers, database) {
    /*
      @params:
        helpers: Access to helper methods
        database: our localStorage DB

      name: module name
      user: current user
      path: assign getPath from container
      oldAbout: store about view to restore it due to cancel action in edit
      data: data keys
      headerClass: instance of Header
      header: header markup
    */
    this.database = database;
    this.helpers = helpers;
    this.name = 'profile';
    this.user = this.helpers.getUser();
    this.path = () => this.helpers.getPath();
    this.oldAbout = '';
    this.data = this.helpers.exportViewVariables('options');
    this.headerClass = new Header(this.name, this.user, helpers);
    this.header = this.headerClass.export();
  }


  /*
    get view, change data keys for user data and append to container
  */
  show() {
    let container = $('.view-wrapper'),
        view,
        markupUrl,
        values = [this.user.name, this.user.email , this.user.webpage, this.user.phone, this.user.direction];

    // create view path and get it with getRemote -> a helper method
    markupUrl = this.path() + 'about.html';
    view = this.helpers.getRemote(markupUrl);

    // Modify data keys for values
    view = this.helpers.insertVars(this.data, values, view);
    
    // Build view with results
    let markup =  `
        ${this.header}
        <main class="flexbox-container flexbox-container--center-horizontal flexbox-container--vertical-start main-section main-section--profile main-section__about">
          ${view}
        </main>
      `;
    // Append to principal container
    return container.html(markup).removeClass('view-wrapper--home view-wrapper--signup').addClass('view-wrapper--profile');
  }

  /*
    get view, change data keys for user data and append to container
  */
  edit() {
    let container,
      editPath = this.path() + 'edit.html',
      markup,
      values = [this.user.name, this.user.email, this.user.webpage, this.user.phone, this.user.direction];

    // Markups and edit functionality are different in Mobile and Desktop, so we verify the screen size first
    if (this.helpers.getSize() == 'mobile') {
      container = $('.data-edit--mobile');

      // Assign current section to re-load it in case of cancel
      this.oldAbout = container.html();

      // create view path and get it with getRemote -> a helper method
      markup = this.helpers.getRemote(editPath);

      // Modify data keys for values
      markup = this.helpers.insertVars(this.data, values, markup);

      return container.html(markup);
    } else {
      // Edit in Desktop is individual, so we get current target, current target value, placeholder and name and use it in our edit container
      let target = $(window.event.target),
          fieldOldValue = target.data('value'),
          fieldPlaceholder = target.data('placeholder'),
          fieldName = target.data('name');

      // Add active to apply style to current option selected
      target.addClass('active');
      target.parent().addClass('active');

      // Change data keys for user data
      fieldOldValue = this.helpers.insertVars(this.data, values, fieldOldValue);

      // Create Edit container
      markup =  `
        <div class="form form--edit-desktop">
          <div class="form_arrow"></div>
          <div class="form__field field">
            <input type="text" class="form__field__input form__field__input--desktop-edit active" value="${fieldOldValue}">
            <label data-name="${fieldName}" class="form__field__title form__field__title--edit-desktop">${fieldPlaceholder}</label>
          </div>
          <button class="default-button default-button--form" id="cancel_save">Cancel</button>
          <button class="default-button default-button--form" id="save">Save</button>
        </div>
      `;

      // Set Parent
      container = target.parent();

      // Delete other edit box that may exist
      $('.form--edit-desktop').remove();

      return container.append(markup);
    }
  }


  /*
    Removes edit box
  */
  cancelEdit() {

    // In mobile, edit replaces settings panel, so we restore it using oldAbout
    if (this.helpers.getSize() == 'mobile') {
      return $('.data-edit--mobile').html(this.oldAbout);
    } else {

      // Remove Box and active classes
      let target = $(window.event.target);
      target.removeClass('active');
      target.parent().removeClass('active');
      return $('.form--edit-desktop').remove();
    }
  }

  /*
    get data and call update method from db
  */
  saveEdit() {

    // Mobile updates all values at once while desktop updates one field at a time, so we check screen width first
    if (this.helpers.getSize() == 'mobile') {

      // Get values and convert them to a stringy json
      let userNew = JSON.stringify({
        email: $('#email').val(),
        name: $('#name').val(),
        direction: $('#direction').val(),
        phone: $('#phone').val(),
        webpage: $('#web-page').val()
      });

      // Array of key names, to match the ones defined in the database Schema
      let fieldName = ['email', 'name', 'direction', 'phone', 'webpage'];

      // return query result
      return this.database.update(this.user.id, userNew, fieldName);
    } else {
      // Get key name and value and convert them to a stringy json
      let fieldName = $('.form__field__title').data('name').toLowerCase(),
          fieldValue = $('.form__field__input').val(),
          userNewField = JSON.stringify({
            [fieldName]: fieldValue
          });

      return this.database.update(this.user.id, userNewField, fieldName);
    }

  }

  /*
    Destroy session
  */
  logout() {
    $.session.set("username", null);
    $.session.clear();
    window.location.replace("");
  }

}
