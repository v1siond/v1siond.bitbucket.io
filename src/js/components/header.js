export default class Header {

  constructor(view = 'login', user = 'na', helpers) {
    /*
      @params:
        view: current view
        user: current user
        helpers: helper methods
      
      markup: empty markup
    */
    this.view = view;
    this.user = user;
    this.helpers = helpers;
    this.markup  =  '';
  }

  export() {
    /*
      We get the current view when we call the clas and export the correct header
    */
    switch(this.view) {
      case 'profile': {
        /*
          Get value keys from helper
        */
        let markupUrl,
            dataKeys = this.helpers.exportViewVariables('header'),
            values = [this.user.name, this.user.reviews, this.user.phone, this.user.direction, this.user.followers];
        /*
          Get view and change keys by user data
          Return view markup
        */
        markupUrl = this.helpers.getPath() + 'header.html';
        this.markup = this.helpers.getRemote(markupUrl)
        this.markup = this.helpers.insertVars(dataKeys, values, this.markup);
        return this.markup;
        break; 
      }
      case 'signup': { 
        return '<header class="header header--signup"><h1 class="title title--home">Signup</h1></header>';
        break; 
      } 
      default: { 
        return '<header class="header header--home"><h1 class="title title--home">Login</h1></header>';
        break; 
      }     
    }
  }
}