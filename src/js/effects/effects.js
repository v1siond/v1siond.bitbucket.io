// ===== Form Inputs =====

/*
  Manage input google-like effect
*/

// Define vars
let input = '.form__field__input',
    placeholder = $('.placeholder'),
    parentInput = '.form__field',
    activeClass = 'active',
    _this;

// if form__field is clicked add active
$(document).on('click', parentInput, (e) => {
  $(e.target).addClass(activeClass); 
});


// Add active on focus
$(document).on('focus', input, (e) => {
  _this = $(e.target)
  _this.parents(parentInput).addClass(activeClass);
  _this.addClass(activeClass);
});

// manage active class on blur
$(document).on('blur', input, () => {
  if (_this.val() == '') {
    _this.parents(parentInput).removeClass(activeClass);
    _this.removeClass(activeClass);
  }
});
