export default class Helpers {

  // Get current user after login
  getUser() {
    try  {
      let user = JSON.parse($.session.get("user"));
      return user;
    } catch(err) {
      return 'na';
    }
  }

  // Export dataKeys for each module
  exportViewVariables(module) {
    if (module == 'header') {
      return ['nameValue', 'reviewsValue', 'phoneValue', 'directionValue', 'followersValue'];
    } else {
      return ['nameValue', 'emailValue', 'webpageValue', 'phoneValue', 'directionValue'];
    }
    
  }

  // Change data-keys for user data
  insertVars(var_strings, vars, view) {
    for (let i=0; i < vars.length; i++) {
      view = view.replace(var_strings[i], vars[i]);
    }
    return view;
  }

  // Get current url folder
  getPath() {
    let userPath = (($(window).width() > 767) ? " src/views/user/desktop/" : "src/views/user/mobile/");
    return userPath;
  }

  // Get view url
  getRemote(url) {
    return $.ajax({
        type: "GET",
        url: url,
        async: false
    }).responseText;
  }

  // Get current screen size
  getSize() {
    let currentWidth = (($(window).width() > 767) ? "desktop" : "mobile");
    return currentWidth;
  }
}